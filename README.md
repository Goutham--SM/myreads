
														# MyReads App - Udacity's first react project

## What is this App
This app is created as a project submission for Udacity's React Fundamentals course. They mostly gave the boiler plate code like the css styling, backend promises and API to get the results. My only work was to modularize the app and try adding conectivity towards all the components and data.

## About MyReads app
This is a book tracking app, which allows the user to track their books under 3 seperate shelves. They are
1. Currently reading shelf: This contains all the books that are being **currently read** by the user.
2. Want to Read shelf: This contains all the books that the user want to read in their near future. This is similar to the wishlist that can be found in a e-commerce site
3. Read shelf: This contains all the books that the user has already completed reading.

## How to use it
To add a book into the shelf of the app, press the green, add fab button at the right bottom corner.
This would take the user to the search page which allows the user to search for books that are stored within the database. There are certain terms and only those terms can be used to search for the books within the database. These keys are
**'Android', 'Art', 'Artificial Intelligence', 'Astronomy', 'Austen', 'Baseball', 'Basketball', 'Bhagat', 'Biography', 'Brief', 'Business', 'Camus', 'Cervantes', 'Christie', 'Classics', 'Comics', 'Cook', 'Cricket', 'Cycling', 'Desai', 'Design', 'Development', 'Digital Marketing', 'Drama', 'Drawing', 'Dumas', 'Education', 'Everything', 'Fantasy', 'Film', 'Finance', 'First', 'Fitness', 'Football', 'Future', 'Games', 'Gandhi', 'Homer', 'Horror', 'Hugo', 'Ibsen', 'Journey', 'Kafka', 'King', 'Lahiri', 'Larsson', 'Learn', 'Literary Fiction', 'Make', 'Manage', 'Marquez', 'Money', 'Mystery', 'Negotiate', 'Painting', 'Philosophy', 'Photography', 'Poetry', 'Production', 'Programming', 'React', 'Redux', 'River', 'Robotics', 'Rowling', 'Satire', 'Science Fiction', 'Shakespeare', 'Singh', 'Swimming', 'Tale', 'Thrun', 'Time', 'Tolstoy', 'Travel', 'Ultimate', 'Virtual Reality', 'Web Development', 'iOS'**
If these key words are not used for searching then it would display the **'No books found'** and the same would also be displayed when there are no books available in the database as per the users query but if these key matches then that is displayed as a result in the same search page.
From the serach page the user will be able to select what book the user wants to move into the 3 different shelfs available.
	- This is achieved by clicking on the grenn button that is present on the book thumbnail, and chosing them as per the users wish.

Once the books are selected they can go the shelves page by clicking on the back arrow that is present near the search bar. This shelves page displays 3 different shelves which displays all the books as per the users intended selection, these books that are being displayed are not static nor constants, they can be moved from one shelf to another as per the users intention.
	* Each book has the following information displayed on it, they are:
    	- The name of the book
        - The cover page or the thumbnail of the cover of the book NOTE: if there cover of the book is not available then the cover will be **'Error'**
        - The author or authors of the book. NOTE: if there are no specific authours or Anonymous authors then the name of the author will be **'Not Mentioned'**



## How To use

To get started developing right away:

* install all project dependencies with `npm install`
* start the development server with `npm start`
* Deploy or build the app with `npm run build`

## What You're Getting
```bash
├── README.md - This file.
├── SEARCH_TERMS.md # The whitelisted short collection of available search terms for you to use with your app.
├── package.json # npm package manager file. It's unlikely that you'll need to modify this.
├── public
│   ├── favicon.ico # React Icon, You may change if you wish.
│   └── index.html # DO NOT MODIFY
└── src
    ├── App.css # Styles for your app. Feel free to customize this as you desire.
    ├── App.js # This is the root of your app. Contains static HTML right now.
    ├── App.test.js # Used for testing. Provided with Create React App. Testing is encouraged, but not required.
    ├── BooksAPI.js # A JavaScript API for the provided Udacity backend. Instructions for the methods are below.
    ├── AuthorTitle.js # This is a component that holds the Name of the author and the title of the book.
    ├── BookShelf.js # This is component that contains the different shelves in which the books would be displayed.
    ├── Books.js # This component holds all the required details to display a single book
    ├── DropdownSelect.js # This component holds the details about the dropdown select menu with which the shelves can be changed.
    ├── SearchBar.js # This componenet just has the neccessary code to enable the search bar feature into the app.
    ├── SearchResult.js # This component is used to display all the results of the search query that the user typed in the search bar.
    ├── Title.js
    ├── icons # Helpful images for your app. Use at your discretion.
    │   ├── add.svg
    │   ├── arrow-back.svg
    │   └── arrow-drop-down.svg
    ├── index.css # Global styles. You probably won't need to change anything here.
    └── index.js # You should not need to modify this file. It is used for DOM rendering only.
```

