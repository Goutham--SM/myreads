import React from 'react';
import DropdownSelect from './DropdownSelect';
import PropTypes from 'prop-types';

import AuthorTitle from './AuthorTitle';

const Books = props => {
  const { book, books, changeShelf } = props;

  // add fallbacks for missing cover images and title
  const bookImg =
    book.imageLinks && book.imageLinks.thumbnail
      ? book.imageLinks.thumbnail
      : 'error';
  const title = book.title ? book.title : 'No title available';
    
    return(
    <li>
    <div className="book">
      <div className="book-top">
        <div className="book-cover" style={{ width: 128, height: 193, backgroundImage: `url(${bookImg})` }}/>
      	<DropdownSelect book={book} books={books} changeShelf={changeShelf}/>
      	</div>
      <AuthorTitle title={title} authors={book.authors} />
	</div>
</li>
    );
  };
Books.propTypes = {
  book: PropTypes.object.isRequired,
  books: PropTypes.array.isRequired,
  changeShelf: PropTypes.func.isRequired
};
export default Books;