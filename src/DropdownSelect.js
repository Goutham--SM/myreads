import React from 'react';
import PropTypes from 'prop-types';

function DropdownSelect(props){
  
  const {book, books, changeShelf} = props;
  
  const updateShelf = event =>
    changeShelf(book, event.target.value);
 
  let currentShelf = 'none';

    // if book is in current list, set current shelf to book.shelf
  	
  	books.forEach(b => b.id===book.id?currentShelf=b.shelf:'none');
   
  return (
    
    <div className="book-shelf-changer">
      <select onChange={updateShelf} defaultValue={currentShelf}>
      <option value="move" disabled>Move to...</option>
      <option value="currentlyReading" >Currently Reading</option>
      <option value="wantToRead">Want to Read</option>
      <option value="read">Read</option>
      <option value="none">None</option>
      </select>
    </div>
    );
}
DropdownSelect.propTypes = {
  book: PropTypes.object.isRequired,
  books: PropTypes.array.isRequired,
  changeShelf: PropTypes.func.isRequired
};
export default DropdownSelect;