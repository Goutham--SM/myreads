import React from 'react'
import {Link} from 'react-router-dom';
// import * as BooksAPI from './BooksAPI'
import './App.css'
import * as BooksAPI from './BooksAPI';
import SearchResult from './SearchResult';


class SearchBar extends React.Component {
  
  state= {
    query:'',
    error:false,
    newBook:[]
  };
	
  handleEvent=(event)=>{
    this.setState({query: event.target.value});
	if (event.target.value) {
      BooksAPI.search(event.target.value.trim(), 20).then(books => {
        books.length > 0
          ? this.setState({ newBook: books, error: false })
          : this.setState({ newBook: [], error: true });
      });

      // if query is empty => reset state to default
    } else this.setState({ newBook: [], error: false });
  };
  
  render(){
    const {books, changeShelf}=this.props;
    return(
    <div className="search-books">
      <div className="search-books-bar">
          <Link className="close-search" to='/'>Close</Link>
          <div className="search-books-input-wrapper">
                {/*
                  NOTES: The search from BooksAPI is limited to a particular set of search terms.
                  You can find these search terms here:
                  https://github.com/udacity/reactnd-project-myreads-starter/blob/master/SEARCH_TERMS.md

                  However, remember that the BooksAPI.search method DOES search by title or author. So, don't worry if
                  you don't find a specific author or title. Every search is limited by search terms.
                */}
          <input type="text" placeholder="Search by title or author" value={this.state.query} onChange={this.handleEvent}/>
          </div>
      </div>
	 <SearchResult books={books} changeShelf={changeShelf} newBook={this.state.newBook} error={this.state.error}/>
	</div>
      );
  };
}

export default SearchBar;