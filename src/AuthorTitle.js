import React from 'react';
import PropTypes from 'prop-types';

function AuthorTitle(props){
   const { title,authors } = props;

  
  return(
    <div>
    	<div className="book-title">{title}</div>
		{
  			authors&&(authors.length>0)?( authors.map((author, index) => (
            <div className="book-authors" key={index}>
              {author}
            </div>
          ))):(
            <div className="book-authors" >
              Not mentioned
            </div>
          )}
    </div>
    );
  
}
AuthorTitle.propTypes = {
    title: PropTypes.string.isRequired,
    authors: PropTypes.array
  };
export default AuthorTitle;