import React from 'react';
import Books from './Books';
import PropTypes from 'prop-types';

function BookShelf (props){
  const {books, changeShelf}= props;

  const bookshelftype = [
      {type: 'currentlyReading', value: 'Currently Reading'},
      {type: 'wantToRead', value: 'Want to Read'},
      {type: 'read', value: 'Read'}
    ];
  
  
    return(
    <div className="list-books-content">
     {bookshelftype.map((shelf,index)=> {
    const shelfBooks = books.filter(b => b.shelf === shelf.type);
      return (
          <div className="bookshelf" key={index}>
            <h2 className="bookshelf-title">{shelf.value}</h2>
            <div className="bookshelf-books">
          <ol className="books-grid">
            {shelfBooks.map( b=> (
       		<Books  book={b}
                    books={books}
                    key={b.id}
                    changeShelf={changeShelf}/>
			))}
          </ol>
        </div>
      </div>
		);
		}
		)}
     </div>
    );
  }
BookShelf.propTypes = {
  books: PropTypes.array.isRequired,
  changeShelf: PropTypes.func.isRequired
};
export default BookShelf;