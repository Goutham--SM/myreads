import React from 'react';
import {BrowserRouter} from 'react-router-dom';
import {Route} from 'react-router-dom';
import {Link} from 'react-router-dom';
import * as BooksAPI from './BooksAPI';

// import * as BooksAPI from './BooksAPI'
import './App.css';
import SearchBar from './SearchBar';
import Title from './Title';
import BookShelf from './BookShelf';
class BooksApp extends React.Component {
  state = {
    /**
    References used to complete this project are:
    	https://www.youtube.com/watch?v=uirRaVjRsf4&list=PLC3y8-rFHvwgg3vaYJgHGnModB54rxOk3&index=11
        Youtube chanel codeEvolution
        https://masteringjs.io/tutorials/fundamentals/foreach-object
        https://reactjs.org/docs/typechecking-with-proptypes.html
        https://stackoverflow.com/questions/42576198/get-object-data-and-target-element-from-onclick-event-in-react-js
        https://css-tricks.com/understanding-react-setstate/
        https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Unexpected_token
        
     * TODO: Instead of using this state variable to keep track of which page
     * we're on, use the URL in the browser's address bar. This will ensure that
     * users can use the browser's back and forward buttons to navigate between
     * pages, as well as provide a good URL they can bookmark and share.
     
     1. Modularize the code as much as possible.
     2. Identify where the data lives
     3. Decide on what type of components must be used should it be a (stateless) functional component or (state) class component
     4. Incorporate the API into the application
     5. Add router to get a good url
     6. Add proptypes to verify that correct type is being used or not.
     */
    books:[]
  };
	
	//Thanks for teaching me that async and await can be used in react whenever we expect a promise or asynchronous processes.
	async componentDidMount() {
    // get books on load
    const books = await BooksAPI.getAll();
	this.setState({ books });
  	}

	  changeShelf = (newOrChangedBook, shelf) =>{
      BooksAPI.update(newOrChangedBook, shelf).then(res =>{
        newOrChangedBook.shelf=shelf;
        this.setState(prevState => ({
        books: prevState.books
          // remove updated book from array
          .filter(book => book.id !== newOrChangedBook.id)
          // add updated book to array
          .concat(newOrChangedBook)
      }));
    });
    };
    
	
  render() {
    const {books} =this.state;
    return (
    <BrowserRouter>
      <div className="app">
        <Route path='/search' render={()=>(
          
            <SearchBar books={books} changeShelf={this.changeShelf}/>

        )}/>
    	<Route exact path='/' render={()=>(
          <div className="list-books">
            <Title/>
              <div>
                <BookShelf books={books} changeShelf={this.changeShelf}/>
               </div>
				<div className="open-search">
                  <Link to="/search">Search</Link>
                </div>
          </div>
        )}/>
      </div>
	  </BrowserRouter>
    )
  }
}

export default BooksApp
