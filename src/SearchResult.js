import React from 'react'
// import * as BooksAPI from './BooksAPI'
import './App.css'
import PropTypes from 'prop-types';

import Books from './Books';

class SearchResult extends React.Component {
  

	render(){
      const { books, changeShelf, newBook, error } =this.props;
    return(
      <div className="search-books-results">
      { newBook.length>0 && (
      	<ol className="books-grid">
       {newBook.map( book=> (
       		<Books  book={book}
                    books={books}
                    key={book.id}
                    changeShelf={changeShelf}/>
       ))}
       </ol>

)}

		{ error&&(
         <h1>No books Found</h1>
         )}
      </div>

      );

};
}

SearchResult.propTypes = {
  error: PropTypes.bool.isRequired,
  books: PropTypes.array.isRequired,
  newBook: PropTypes.array.isRequired,
  changeShelf: PropTypes.func.isRequired
};
export default SearchResult;